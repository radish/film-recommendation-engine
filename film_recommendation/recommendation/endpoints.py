import operator

import numpy as np
import rethinkdb as r
from rethinkframework.contrib.auth.permissions import IsAuthenticated
from rethinkframework.server.routes import HTTPEndpoint, Request, json_response

from film_recommendation.authentication.models import User
from film_recommendation.films.models import Film


class InitialRatings(HTTPEndpoint):
    permissions = (IsAuthenticated,)

    async def on_get(self, request: Request):
        votes_nums = []
        votes_sum = 0

        # This returns chosen number of films minus secondary index duplicates
        user = request.get('user')

        try:
            films_ids = set(
                map(lambda vote: vote['film_id'], user['film_votes'])
            )
        except KeyError:
            films_ids = set()

        try:
            films_ids.union(user['films_saved'])
        except KeyError:
            pass

        films = await (Film.documents
                       .order_by(index=r.desc('votes_number'))
                       .filter(lambda doc: r.expr(films_ids)
                               .contains(doc['movielens_id'])
                               .not_())
                       .limit(500).distinct().execute())

        for film in films:
            votes_num = film['votes_number']
            votes_nums.append(votes_num)
            votes_sum += votes_num

        probabilities = [votes_num / votes_sum for votes_num in votes_nums]

        choices = np.random.choice(films, 30, False, p=probabilities).tolist()

        return json_response(choices)


class RateFilms(HTTPEndpoint):
    permissions = (IsAuthenticated,)

    async def on_get(self, request: Request):
        user = request.get('user')

        if 'film_votes' in user:
            return json_response(user['film_votes'])
        else:
            return json_response([])

    async def on_post(self, request: Request):
        json_request = await request.json()
        user = request.get('user')

        if 'film_votes' in json_request:
            new_votes = json_request['film_votes']
            # Add votes to user
            if 'film_votes' in user:
                user_votes = user['film_votes']
                intersect = [vote for vote in new_votes if vote in user_votes]
                if len(intersect) != 0:
                    return json_response({
                        'details': 'There is a duplicate on your votes list'
                    })
                user_votes.extend(new_votes)
            else:
                user['film_votes'] = new_votes

            # Gets new films ids
            new_films_ids = []
            shortcut_rating = {}
            if 'films_saved' in user:
                saved_films = user['films_saved']
            else:
                saved_films = []
            for film_dict in new_votes:
                if film_dict['film_id'] in saved_films:
                    saved_films.remove(film_dict['film_id'])

                new_films_ids.append(film_dict['film_id'])
                shortcut_rating[film_dict['film_id']] = film_dict['rating']

            # Gets new films details
            rated_films = await (Film.documents.filter(lambda doc: r.expr(new_films_ids)
                                                       .contains(doc['movielens_id'])).execute())
            if 'film_genres' in user:
                user_film_genres = user['film_genres']
            else:
                user_film_genres = []

            if 'film_actors' in user:
                user_film_actors = user['film_actors']
            else:
                user_film_actors = []

            if 'film_directors' in user:
                user_film_directors = user['film_directors']
            else:
                user_film_directors = []

            if 'film_writers' in user:
                user_film_writers = user['film_writers']
            else:
                user_film_writers = []

            if 'film_years' in user:
                user_film_years = user['film_years']
            else:
                user_film_years = []

            if 'film_keywords' in user:
                user_film_keywords = user['film_keywords']
            else:
                user_film_keywords = []

            # Counting votes for all genres, actors, directors, writers from rated films
            while await rated_films.fetch_next():
                rated_film = await rated_films.next()
                RateFilms.count_points('genres', rated_film, shortcut_rating, user_film_genres)
                RateFilms.count_points('actors', rated_film, shortcut_rating, user_film_actors)
                RateFilms.count_points('directors', rated_film, shortcut_rating, user_film_directors)
                RateFilms.count_points('writers', rated_film, shortcut_rating, user_film_writers)
                RateFilms.count_year_points(rated_film, shortcut_rating, user_film_years)
                RateFilms.count_keyword_points(rated_film, shortcut_rating, user_film_keywords)

            user['film_genres'] = user_film_genres
            user['film_actors'] = user_film_actors
            user['film_directors'] = user_film_directors
            user['film_writers'] = user_film_writers
            user['film_years'] = user_film_years
            user['film_keywords'] = user_film_keywords
            user['films_saved'] = saved_films

            await User.documents.update(user).execute()
            return json_response({
                'details': 'Ratings successful.'
            })
        else:
            return json_response({
                'details': 'In request there is not any new votes for films.'
            })

    @staticmethod
    def points_for_rate(rate: int) -> int:
        if rate == 1:
            return -15
        elif rate == 2:
            return -5
        elif rate == 3:
            return 5
        elif rate == 4:
            return 10
        elif rate == 5:
            return 15
        elif rate == -1:
            return -30
        else:
            return 0

    @staticmethod
    def count_points(type_name, rated_film, shortcut_rating, user_votes):
        singular_type = type_name[:-1]

        if bool(user_votes):
            for type_element in rated_film[type_name]:
                if any(element_details[singular_type] == type_element for element_details in
                       user_votes):  # check is any genre_votes for this film genre
                    for element_details in user_votes:
                        if element_details[singular_type] == type_element:
                            element_details['rating'] += RateFilms.points_for_rate(shortcut_rating
                                                                                   [rated_film['movielens_id']])
                            element_details['votes_quantity'] += 1
                            break

                else:
                    element_details = {singular_type: type_element,
                                       'rating': RateFilms.points_for_rate(
                                           shortcut_rating[rated_film['movielens_id']]),
                                       'votes_quantity': 1}
                    user_votes.append(element_details)
        else:
            for type_element in rated_film[type_name]:
                element_details = {singular_type: type_element,
                                   'rating': RateFilms.points_for_rate(
                                       shortcut_rating[rated_film['movielens_id']]),
                                   'votes_quantity': 1}
                user_votes.append(element_details)

    @staticmethod
    def count_year_points(rated_film, shortcut_rating, user_votes):
        if 'year' in rated_film:
            year = str(rated_film['year'])
            x = year[-1:]
            last_digit = int(year[-1:])  # get last char
            year = int(year[:3] + '0')  # change year to int and replace last digit of year digit to 0
        else:
            return
        if last_digit < 3:
            neighbor_year = -10
        elif last_digit > 7:
            neighbor_year = 10
        else:
            neighbor_year = 0

        if bool(user_votes):
            if any(element_details['year'] == year for element_details in
                   user_votes):
                for element_details in user_votes:
                    if element_details['year'] == year:
                        element_details['rating'] += RateFilms.points_for_rate(shortcut_rating
                                                                               [rated_film['movielens_id']])
                        element_details['votes_quantity'] += 1
                        break

                if neighbor_year != 0:
                    if any(element_details['year'] == year + neighbor_year for element_details in
                           user_votes):
                        for element_details in user_votes:
                            if element_details['year'] == year + neighbor_year:
                                element_details['rating'] += RateFilms.points_for_rate(shortcut_rating
                                                                                       [rated_film['movielens_id']]) / 2
                                break
                    else:
                        neighboring_years = {'year': year + neighbor_year,
                                             'rating': RateFilms.points_for_rate(
                                                 shortcut_rating[rated_film['movielens_id']]) / 2,
                                             'votes_quantity': 1}
                        user_votes.append(neighboring_years)
                        return
            else:
                main_year = {'year': year,
                             'rating': RateFilms.points_for_rate(
                                 shortcut_rating[rated_film['movielens_id']]),
                             'votes_quantity': 1}
                user_votes.append(main_year)

                if neighbor_year != 0:
                    if any(element_details['year'] == year + neighbor_year for element_details in
                           user_votes):
                        for element_details in user_votes:
                            if element_details['year'] == year + neighbor_year:
                                element_details['rating'] += RateFilms.points_for_rate(shortcut_rating
                                                                                       [rated_film['movielens_id']]) / 2
                                break
                    else:
                        neighboring_years = {'year': year + neighbor_year,
                                             'rating': RateFilms.points_for_rate(
                                                 shortcut_rating[rated_film['movielens_id']]) / 2,
                                             'votes_quantity': 1}
                        user_votes.append(neighboring_years)
                        return
                else:
                    return

        else:
            main_year = {'year': year,
                         'rating': RateFilms.points_for_rate(
                             shortcut_rating[rated_film['movielens_id']]),
                         'votes_quantity': 1}
            user_votes.append(main_year)

            if neighbor_year != 0:
                neighboring_years = {'year': year + neighbor_year,
                                     'rating': RateFilms.points_for_rate(
                                         shortcut_rating[rated_film['movielens_id']]) / 2,
                                     'votes_quantity': 1}
                user_votes.append(neighboring_years)

    @staticmethod
    def count_keyword_points(rated_film, shortcut_rating, user_keywords):
        max_movie_score = 10
        if user_keywords:
            for keyword_element in rated_film['keywords']:
                for keyword_details in user_keywords:
                    if keyword_details['keyword'] == keyword_element['keyword']:
                        keyword_details['rating'] += RateFilms.points_for_rate(
                            shortcut_rating[rated_film['movielens_id']]
                        ) * (keyword_element['score'] / max_movie_score)
                        keyword_details['votes_quantity'] += 1
                        break

                # Keyword not found
                keyword_details = {
                    'keyword': keyword_element['keyword'],
                    'rating': RateFilms.points_for_rate(
                        shortcut_rating[rated_film['movielens_id']]
                    ) * (keyword_element['score'] / max_movie_score),
                    'votes_quantity': 1
                }
                user_keywords.append(keyword_details)
        else:
            for keyword_element in rated_film['keywords']:
                keyword_details = {
                    'keyword': keyword_element['keyword'],
                    'rating': RateFilms.points_for_rate(
                        shortcut_rating[rated_film['movielens_id']]
                    ) * (keyword_element['score'] / max_movie_score),
                    'votes_quantity': 1
                }
                user_keywords.append(keyword_details)


class SaveFilms(HTTPEndpoint):
    permissions = (IsAuthenticated,)

    async def on_get(self, request: Request):
        user = request.get('user')

        if 'films_saved' in user:
            films_id = user['films_saved']
            saved_films = await (
            Film.documents.filter(lambda doc: r.expr(films_id).contains(doc['movielens_id'])).execute())
            films = []
            while await saved_films.fetch_next():
                saved_film = await saved_films.next()
                films.append(saved_film)

            return json_response(films)
        else:
            return json_response([])

    async def on_post(self, request: Request):
        json_request = await request.json()
        user = request.get('user')

        if 'films_saved' in json_request:
            if 'films_saved' in user:
                films_id = user['films_saved']
            else:
                films_id = []
            films_id.extend(json_request['films_saved'])
            user['films_saved'] = films_id
            await User.documents.update(user).execute()
            return json_response({
                'details': 'Saving files successful.'
            })
        else:
            return json_response([])


class RecommendedFilm(HTTPEndpoint):
    permissions = (IsAuthenticated,)

    async def on_get(self, request: Request):
        films_id = []
        films_genres = []
        films_actors = []
        film_directors = []
        film_writers = []
        film_years = []
        film_keywords = []

        points_compartments = {'genres': 30,
                               'keywords': 10,
                               'year': 10,
                               'votes_position': 10,
                               'actors': 10,
                               'directors': 10,
                               'writers': 10}

        user = request.get('user')
        if 'film_votes' in user:
            user_votes = user['film_votes']
            for film_vote in user_votes:
                films_id.append(film_vote['film_id'])
            if 'film_genres' in user.keys():
                films_genres = RecommendedFilm.prepare_shortcut_information(user['film_genres'], 'genres')
            if 'film_actors' in user.keys():
                films_actors = RecommendedFilm.prepare_shortcut_information(user['film_actors'], 'actors')
            if 'film_directors' in user.keys():
                film_directors = RecommendedFilm.prepare_shortcut_information(user['film_directors'], 'directors')
            if 'film_writers' in user.keys():
                film_writers = RecommendedFilm.prepare_shortcut_information(user['film_writers'], 'writers')
            if 'film_years' in user.keys():
                film_years = RecommendedFilm.prepare_shortcut_information(user['film_years'], 'years')
            if 'film_keywords' in user.keys():
                film_keywords = RecommendedFilm.prepare_shortcut_information(user['film_keywords'], 'keywords')

        films_id = list(set(films_id))  # remove duplicates
        if 'films_saved' in user:
            films_id.extend(user['films_saved'])
        available_films = await (Film.documents.filter(lambda doc: r.expr(films_id).contains(doc['movielens_id'])
                                                       .not_()).execute())
        films_points = {}
        while await available_films.fetch_next():
            available_film = await available_films.next()

            year = [int(str(available_film['year'])[0:3] + '0')]  # round last year digit
            film_rating = 0
            if 'rating' in available_film.keys():
                film_rating = available_film['rating']

            genres_points = RecommendedFilm.count_points(available_film['genres'], films_genres)
            actors_points = RecommendedFilm.count_points(available_film['actors'], films_actors)
            directors_points = RecommendedFilm.count_points(available_film['directors'], film_directors)
            writers_points = RecommendedFilm.count_points(available_film['writers'], film_writers)
            votes_position_points = np.power(0.9995, available_film['votes_position'])
            votes_years_point = RecommendedFilm.count_points(year, film_years)
            keywords_points = RecommendedFilm.count_keywords_points(available_film['keywords'], film_keywords)
            films_points[available_film['movielens_id']] = (
                genres_points * points_compartments['genres'] +
                actors_points * points_compartments['actors'] +
                directors_points * points_compartments['directors'] +
                writers_points * points_compartments['writers'] +
                film_rating +
                votes_position_points * points_compartments['votes_position'] +
                votes_years_point * points_compartments['year'] +
                keywords_points * points_compartments['keywords']
            )

        number_of_films = 10
        # compare the performance of sorted with heapq.nlargest
        recommended_films_id = [x[0] for x in (
            sorted(films_points.items(), key=operator.itemgetter(1), reverse=True)[:number_of_films])]

        recommended_films = await (
            Film.documents.filter(lambda doc: r.expr(recommended_films_id).contains(doc['movielens_id'])).execute())

        returned_films = []
        while await recommended_films.fetch_next():
            recommended_film = await recommended_films.next()
            returned_films.append(recommended_film)
        return json_response(returned_films)

    @staticmethod
    def prepare_shortcut_information(user_votes, type_name):
        singular_type = type_name[:-1]

        return {
            vote[singular_type]: vote['rating'] / vote['votes_quantity']
            for vote in user_votes
        }

    @staticmethod
    def count_points(element_list, user_marks):
        counter = 0
        points = 0

        if not user_marks:
            return 0

        for element in element_list:
            if element in user_marks.keys():
                counter += 1
                points += user_marks[element]

        sorted_dict = dict(sorted(user_marks.items(), key=operator.itemgetter(1), reverse=True)[:counter])

        denominator = 0
        for element in sorted_dict:
            denominator += sorted_dict[element]

        return 0 if denominator == 0 else points / denominator

    @staticmethod
    def count_keywords_points(element_list, user_marks):
        counter = 0
        points = 0

        if not user_marks:
            return 0

        for element in element_list:
                if element['keyword'] in user_marks.keys():
                    counter += 1
                    points += user_marks[element['keyword']]
        sorted_dict = dict(sorted(user_marks.items(),
                                  key=operator.itemgetter(1),
                                  reverse=True)[:counter])

        denominator = 0
        for element in sorted_dict:
            denominator += sorted_dict[element]

        return 0 if denominator == 0 else points / denominator
