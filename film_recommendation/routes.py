from rethinkframework.contrib.auth import endpoints as auth
from rethinkframework.server.routes import Route

from film_recommendation.films import endpoints as films
from film_recommendation.recommendation import endpoints as recommendation

routes_list = [
    Route('/v0/recommendation/initial_ratings', recommendation.InitialRatings),
    Route('/v0/auth/login', auth.LoginEndpoint),
    Route('/v0/auth/register', auth.RegisterEndpoint),
    Route('/v0/film/{id}', films.FilmInfo),
    Route('/v0/search_film/{name}', films.FilmSearch),
    Route('/v0/recommendation/rate_films', recommendation.RateFilms),
    Route('/v0/recommendation/recommended_films',
          recommendation.RecommendedFilm),
    Route('/v0/recommendation/save_films', recommendation.SaveFilms)
]
