from rethinkframework.db import models


class Film(models.DocumentModel):
    __table_name__ = 'films'
    __primary_key__ = 'movielens_id'
    __secondary_indexes__ = {
        'votes_number': (),
    }

    __schema__ = {
        'movielens_id': {'type': 'number'},
        'imdb_id': {'type': 'string'},
        'title': {'type': 'string'},
        'year': {'type': 'string'},
        'poster': {'type': 'string'},
        'genres': {'type': 'list', 'schema': {'type': 'string'}},
        'plot': {'type': 'string'},
        'directors': {'type': 'list', 'schema': {'type': 'string'}},
        'writers': {'type': 'list', 'schema': {'type': 'string'}},
        'actors': {'type': 'list', 'schema': {'type': 'string'}},
        'rating': {'type': 'number'},
        'votes_number': {'type': 'number'},
        'votes_position': {'type': 'number'},
        'keywords': {'type': 'list', 'schema': {'type': 'string'}}
    }


class Keywords(models.DocumentModel):
    __table_name__ = 'keywords'
    __primary_key__ = 'keyword'

    __schema__ = {
        'keyword': {'type': 'string'},
        'count': {'type': 'number'}
    }
