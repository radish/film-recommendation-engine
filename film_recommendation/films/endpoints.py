import rethinkdb as r
from aiohttp import errors
from rethinkframework.contrib.auth.permissions import AllowAny
from rethinkframework.server.routes import HTTPEndpoint, Request, json_response

from film_recommendation.films.models import Film


class FilmInfo(HTTPEndpoint):
    permissions = (AllowAny,)

    async def on_get(self, request: Request):
        try:
            film_id = int(request.match_info['id'])
            film = await Film.documents.get(film_id).execute()

            if film is not None:
                return json_response(film)

            return json_response({'detail': 'Film does not exist'},
                                 status=errors.HttpBadRequest.code)
        except KeyError:
            return json_response({'detail': 'Film ID not provided'},
                                 status=errors.HttpBadRequest.code)


class FilmSearch(HTTPEndpoint):
    permissions = (AllowAny,)

    async def on_get(self, request: Request):
        name = request.match_info['name'].replace('_', ' ')
        cursor = await (Film.documents
                            .filter(r.row['title'].match('(?i)' + name))
                            .execute())

        films = []
        while await cursor.fetch_next():
            films.append(await cursor.next())

        return json_response(films)
