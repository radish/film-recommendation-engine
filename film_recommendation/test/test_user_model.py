import pytest
from rethinkframework.db.exceptions import ValidationError

from film_recommendation.db.models import User


def test_valid_creation():
    User(email='test@test.com', password='test')


def test_creation_without_email():
    with pytest.raises(ValidationError):
        User(password='test')


def test_creation_without_password():
    with pytest.raises(ValidationError):
        User(email='test@test.com')
