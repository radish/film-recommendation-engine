import os
import pytest

os.environ['RTF_SETTINGS'] = 'film_recommendation.settings'


@pytest.yield_fixture(scope='session')
def server_address():
    from rethinkframework.management.commands_handling import run_test_server

    thread = next(run_test_server())
    yield '127.0.0.1:8001'

    thread.stop()
    thread.join()
