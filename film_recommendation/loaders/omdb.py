import asyncio

import aiohttp


class OmdbLoader:
    _api_address = 'http://www.omdbapi.com'
    _used_keys = ('imdbRating', 'Genre', 'Poster', 'Title', 'Director',
                  'Writer', 'Plot', 'Actors', 'Year')
    _keys_remaps = {
        'imdbRating': 'rating',
        'Genre': 'genres',
        'Poster': 'poster',
        'Title': 'title',
        'Director': 'directors',
        'Writer': 'writers',
        'Plot': 'plot',
        'Actors': 'actors',
        'Year': 'year'
    }

    def __init__(self):
        self._session = aiohttp.ClientSession()
        self._semaphore = asyncio.Semaphore(1000)

    async def close(self):
        await self._session.close()

    def _filter_keys(self, dict_to_filter):
        return {self._keys_remaps[key]: dict_to_filter[key] for key in
                self._used_keys}

    async def get_by_title(self, title, year='', plot='short'):
        if plot.lower() not in ('short', 'long'):
            raise ValueError('Plot can only be set to short or long')

        while True:
            async with self._semaphore:
                try:
                    async with self._session.get(
                        self._api_address,
                        params={'t': title, 'y': year, 'plot': plot}
                    ) as response:
                        return self._filter_keys(await response.json())
                except aiohttp.ClientOSError:
                    pass

    async def get_by_id(self, film_id, plot='short'):
        if plot.lower() not in ('short', 'full'):
            raise ValueError('Plot can only be set to short or long')

        while True:
            async with self._semaphore:
                try:
                    async with self._session.get(
                            self._api_address,
                            params={'i': film_id, 'plot': plot}
                    ) as response:
                        return self._filter_keys(await response.json())
                except aiohttp.ClientOSError:
                    pass
