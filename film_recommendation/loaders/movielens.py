import asyncio
import csv
import os
from concurrent.futures import ThreadPoolExecutor
from typing import Dict, Any


def find(func, collection):
    for item in collection:
        if func(item) is True:
            return item

    raise LookupError('Not found')


class MovieLensLoader:
    def __init__(self, path: str, loop: asyncio.AbstractEventLoop):
        self.__path = path
        self.__ratings = []
        self.__films = []
        self.__loop = loop
        self.__executor = ThreadPoolExecutor()
        self.__links = self.__load_links()
        self.__films_file = open(os.path.join(self.__path, 'movies.csv'),
                                 encoding='utf8')
        self.__films_reader = csv.DictReader(
            self.__films_file,
            ('movielens_id', 'title', 'genres')
        )
        next(self.__films_reader)

    def __load_links(self) -> Dict[str, Any]:
        links = {}

        with open(os.path.join(self.__path, 'links.csv'),
                  encoding='utf8') as links_file:
            links_reader = csv.DictReader(
                links_file,
                ('movielens_id', 'imdb_id', 'tmdb_id')
            )
            next(links_reader)

            for link in links_reader:
                del link['tmdb_id']
                links[link.pop('movielens_id')] = link

        return links

    def __get_next_film(self) -> Dict[str, Any]:
        film = next(self.__films_reader)

        if film['genres'] == '(no genres listed)':
            film['genres'] = set()
        else:
            film['genres'] = set(film['genres'].split('|'))

        return {**film, **self.__links[film['movielens_id']]}

    async def __aiter__(self) -> 'MovieLensLoader':
        return self

    async def __anext__(self) -> Dict[str, Any]:
        film = await self.__loop.run_in_executor(self.__executor,
                                                 self.__get_next_film)

        if film is not None:
            return film

        raise StopAsyncIteration
