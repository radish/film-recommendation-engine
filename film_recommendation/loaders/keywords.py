import asyncio
import os
from concurrent.futures import ProcessPoolExecutor
from tempfile import TemporaryDirectory

import nltk
from rethinkframework.db.connection import DatabaseConnection

from film_recommendation.films.models import Film


class KeywordsLoader:
    _NTLK_TMP_DIR = TemporaryDirectory(prefix='nltk_')

    @staticmethod
    def _calculate_score(keywords, words_statistics):
        scored_keywords = []
        for keyword in keywords:
            score = 10 * 0.999 ** words_statistics[keyword]
            scored_keywords.append({"keyword": keyword, "score": score})

        return scored_keywords

    @staticmethod
    def _gather_keywords(film):
        nouns = []
        text = nltk.word_tokenize(film["plot"])
        text_pos = nltk.pos_tag(text)
        for (word, pos) in text_pos:
            if pos == 'NN':
                nouns.append(word)

        film_id = film["movielens_id"]

        return film_id, nouns

    @staticmethod
    async def _insert_keywords_into_db(film_id, film_keywords):
        await Film.documents.get(film_id).update({'keywords': film_keywords}).execute()

    def __init__(self, event_loop):
        self.__event_loop = event_loop

    @staticmethod
    def _download_nltk():
        nltk_tmp_dir = KeywordsLoader._NTLK_TMP_DIR.name
        nltk.data.path.append(nltk_tmp_dir)
        nltk.download('abc', download_dir=nltk_tmp_dir)
        nltk.download('averaged_perceptron_tagger', download_dir=nltk_tmp_dir)
        nltk.download('dependency_treebank', download_dir=nltk_tmp_dir)
        nltk.download('ptb', download_dir=nltk_tmp_dir)
        nltk.download('treebank', download_dir=nltk_tmp_dir)
        nltk.download('punkt', download_dir=nltk_tmp_dir)
        nltk.download('hmm_treebank_pos_tagger', download_dir=nltk_tmp_dir)

    async def process_keywords(self):
        KeywordsLoader._download_nltk()
        os.environ.setdefault('RTF_SETTINGS', 'film_recommendation.settings')
        from rethinkframework.loaders import settings

        await DatabaseConnection.setup_db(settings.DATABASE)

        words_statistics = {}
        films_keywords = {}
        films = await Film.documents.execute()

        films_count = await Film.documents.count().execute()

        print("Gathering keywords...")
        executor = ProcessPoolExecutor()

        done_count = 0
        words_count = 0
        tasks = set()

        while await films.fetch_next():
            film = await films.next()
            tasks.add(self.__event_loop.run_in_executor(executor, KeywordsLoader._gather_keywords, film))

            done, tasks = await asyncio.wait(tasks, loop=self.__event_loop, timeout=0.0005)

            # done is set of Future, so we need call result() to get result
            for task_retval in done:
                film_id, words = task_retval.result()
                films_keywords[film_id] = words
                for word in words:
                    if word in words_statistics:
                        words_statistics[word] += 1
                        words_count += 1
                    else:
                        words_statistics[word] = 1
                        words_count += 1

            done_count += len(done)
            print('\r>> {}/{}'.format(done_count, films_count), end='')

        done = await asyncio.gather(*tasks)
        done_count += len(done)
        print('\r>> {}/{}'.format(done_count, films_count), end='')

        # done is list of results, so task_retval is tuple
        for task_retval in done:
            film_id, words = task_retval
            films_keywords[film_id] = words
            for word in words:
                if word in words_statistics:
                    words_statistics[word] += 1
                    words_count += 1
                else:
                    words_statistics[word] = 1
                    words_count += 1

        print('\r>> {}/{} Done!'.format(done_count, films_count))

        tasks = set()
        done_count = 0

        print('Filtering words with low occurence number...')
        allowed_keywords = {k: v for k, v in words_statistics.items() if v >= 10}

        print('Filtering filtered keywords from films and updating database...')
        for film_id, keywords in films_keywords.items():
            films_keywords[film_id] = set([word for word in keywords if word in
                                           allowed_keywords])
            scored_film_keywords = KeywordsLoader._calculate_score(films_keywords[film_id], words_statistics)

            tasks.add(KeywordsLoader._insert_keywords_into_db(film_id, scored_film_keywords))
            done, tasks = await asyncio.wait(tasks, loop=self.__event_loop, timeout=0.001)
            done_count += len(done)
            print('\r>> {}/{}'.format(done_count, films_count), end='')

        done = await asyncio.gather(*tasks)
        done_count += len(done)
        print('\r>> {}/{} Done!'.format(done_count, films_count))

        tasks = set()
        done_count = 0
        keywords_count = len(allowed_keywords)

        await DatabaseConnection.close_connection()

if __name__ == "__main__":
    event_loop = asyncio.get_event_loop()
    keywords = KeywordsLoader(event_loop)
    event_loop.run_until_complete(keywords.process_keywords())
