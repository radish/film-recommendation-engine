import csv
from collections import defaultdict
from csv import DictReader


class VotesAnalyzer:
    @staticmethod
    def generate_number_of_votes_file(ratings_path, movies_path, output_path):
        films = defaultdict(int)

        with open(ratings_path, 'r', encoding='utf8') as ratings_file:
            ratings_reader = DictReader(ratings_file)
            for ml_rating in ratings_reader:
                films[ml_rating['movieId']] += 1

        with open(movies_path, 'r', encoding='utf8') as films_file:
            movies_reader = DictReader(films_file)
            titles = {film['movieId']: film['title'] for film in movies_reader}

        with open(output_path, 'w', newline='', encoding='utf8') as out_file:
            data = csv.writer(out_file)
            data.writerow(('movieId', 'votes', 'title'))

            for key, film in films.items():
                data.writerow((key, film, titles[key]))

    @staticmethod
    def get_dict_movies_and_votes(file_path):
        films = {}

        with open(file_path, 'r', encoding='utf8') as films_file:
            films_reader = DictReader(films_file)
            for ml_film in films_reader:
                films[ml_film['movieId']] = int(ml_film['votes'])

        return films
