from rethinkframework.contrib.auth.auth_middlewares import jwt_auth_middleware

SECRET_KEY = 'testkey'

APP_NAME = 'film_recommendation'

# Routes main file
ROOT_ROUTES = 'film_recommendation.routes'


# List of modules containing models
MODULES = [
    'film_recommendation.authentication',
    'film_recommendation.films'
]

# Database connection data
DATABASE = {
    'host': 'localhost',
    'port': 28015,
    'db': 'film_recommendation',
    'auth_key': ''
}

# Authorization
AUTH_MIDDLEWARE = jwt_auth_middleware

# JWT authorization settings
JWT_ALGORITHM = 'HS256'
JWT_SECRET = 'testsecret'
JWT_EXP_DELTA_SECONDS = 31536000

# User model
USER_MODEL = 'film_recommendation.authentication.models.User'
