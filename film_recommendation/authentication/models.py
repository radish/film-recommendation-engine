from rethinkframework.contrib.auth.models import User as BaseUser
from rethinkframework.contrib.db.interceptors import PasswordInterceptor


class User(BaseUser):
    __schema__ = {
        'email': {'type': 'string', 'required': True},
        'password': {**PasswordInterceptor.__schema__, 'required': True},
        'film_votes': {'type': 'list', 'schema': {
            'type': 'dict', 'schema': {
                'film_id': {'type': 'number'},
                'rating': {'type': 'number'}
            }
        }},
        'film_genres': {'type': 'list', 'schema': {
            'type': 'dict', 'schema': {
                'genre': {'type': 'number'},
                'rating': {'type': 'number'},
                'votes_quantity': {'type': 'number'}
            }
        }},
        'film_actors': {'type': 'list', 'schema': {
            'type': 'dict', 'schema': {
                'actor': {'type': 'number'},
                'rating': {'type': 'number'},
                'votes_quantity': {'type': 'number'}
            }
        }},
        'film_directors': {'type': 'list', 'schema': {
            'type': 'dict', 'schema': {
                'director': {'type': 'number'},
                'rating': {'type': 'number'},
                'votes_quantity': {'type': 'number'}
            }
        }},
        'film_writers': {'type': 'list', 'schema': {
            'type': 'dict', 'schema': {
                'writer': {'type': 'number'},
                'rating': {'type': 'number'},
                'votes_quantity': {'type': 'number'}
            }
        }},
    }
