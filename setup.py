#!/usr/bin/env python3

from setuptools import setup, find_packages

from film_recommendation import __version__

setup(
    name='film-recommendation-engine',
    version=__version__,
    url='https://gitlab.com/radish/film-recommendation-engine',
    install_requires=['RethinkFramework==0.1.0.dev3', 'numpy', 'nltk'],
    packages=find_packages(),
    scripts=['bin/FREFilmLoader', 'bin/FREServerManager', ]
)
