# Prerequisites 
    * git
    * pip
    * RethinkDB >= 2.3.1

# Installation

## Download project

    git clone https://gitlab.com/radish/film-recommendation-engine.git

## Install RethinkFramework
    
    pip install --process-dependency-links  'git+https://gitlab.com/rivi/RethinkFramework.git' --upgrade

## Install project

In project directory:

    pip install .

## Configure database connection

In `film_recommendation/settings.py` file in line No. 18 set correct RethinkDB connection setting, i.e.

    DATABASE = {
        'host': 'localhost',
        'port': 28015,
        'db': 'film_recommendation',
        'auth_key': ''
    }

# Database data bootstrap

## Create database schema

Run `FREServerManager` with argument `migrate`

In project directory:

    python bin/FREServerManager migrate

## Download latest full MovieLens Dataset (`ml-latest.zip`)

MovieLens Dataset link: `http://grouplens.org/datasets/movielens/latest/`

## Unpack MovieLens Dataset

## Download and populate database with data

Run `FREFilmLoader <directory_with_unpacked_movielens_dataset>`

    python bin/FREFilmLoader ml-latest

# Testing of recommendation

To test recommendation system use a client in examples folder.

## Run Film Recommendation Engine server

In project directory, run Film Recommendation Engine server:

    python bin/FREServerManager run_server

Use `python bin/FREServerManager --help` for usage.

## Run console client

Run as script `examples/FREcli` to start use recommendation system.

# Reloading keywords and changing to RAKE algorithm (optional)

If you want to use the other keywords gathering systems, follow these steps:

## Change branch to `master_with_rake`

    git checkout master_with_rake

## Update database with new keywords

Run file `film_recommendation/loaders/keywords.py` as script.

In project directory:

    python -m film_recommendation.loaders.keywords

# Standalone implementation of RAKE algorithm

There is standalone implementation of RAKE algorithm in branch `master_with_rake` in file film_recommendation/loaders/rake.py.
Run it as script for testing. In case of failure of downloading nltk, remove nltk directory from temporary folder (`%TEMP%` on Windows, `/tmp` on Linux).
